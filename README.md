# 📖 Book register app with Spring. 📖

### Project info
This is a demo application for registering and managing books and their authors in your local computer.

### Main technologies used in project
* ***Framework:*** Spring Boot 2.3.3 (with Java 1.8)
* ***Back end:*** PostgreSQL
* ***Front end:*** React

### Branches:
* ***main:*** Used throughout development. Should not be used by end users.
* ***release:*** Contains stable and released version(s).
