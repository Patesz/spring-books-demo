package hu.ricky.books.api;

import hu.ricky.books.exception.ResourceNotFoundException;
import hu.ricky.books.model.Book;
import hu.ricky.books.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/book")
public class BookController {

    private final BookRepository bookRepository;

    @Autowired(required = true)
    public BookController(BookRepository bookRepository){
        this.bookRepository = bookRepository;
    }

    @PostMapping
    public String save(@RequestBody Book book){
        try {
            bookRepository.save(book);
            return "Successful insert";
        } catch (Exception e) {
            e.printStackTrace();
            return "Unsuccessful insert";
        }

    }

    @GetMapping
    public List<Book> findAll(){
        return bookRepository.findAll();
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<Book> findById(@PathVariable("id") Long id) throws ResourceNotFoundException {
        Book value = bookRepository.findById(id);
        return ResponseEntity.ok().body(value);
    }

    @DeleteMapping("{id}")
    public String delete(@PathVariable(value = "id") Long id) {
        try {
            bookRepository.delete(id);
            return "Successful delete with id " + id;
        } catch (Exception e) {
            e.printStackTrace();
            return "Unsuccessful delete with id " + id;
        }
    }

    @PutMapping("{id}")
    public String update(@PathVariable(value = "id") Long id, @RequestBody Book newBook) {

        try {
            Book bookToUpdate = bookRepository.findById(id);

            bookToUpdate.setIsbn(newBook.getIsbn());
            bookToUpdate.setTitle(newBook.getTitle());
            bookToUpdate.setPages(newBook.getPages());
            bookToUpdate.setWrittenIn(newBook.getWrittenIn());
            bookToUpdate.setGenre(newBook.getGenre());

            bookRepository.update(bookToUpdate);

            return "Successful update with id " + id;
        } catch (Exception e) {
            e.printStackTrace();
            return "Unsuccessful update with id " + id;
        }
    }
}
