package hu.ricky.books.api;

import hu.ricky.books.exception.ResourceNotFoundException;
import hu.ricky.books.model.Author;
import hu.ricky.books.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/author")
public class AuthorController {

    private final AuthorRepository authorRepository;

    @Autowired(required = true)
    public AuthorController(AuthorRepository authorRepository){
        this.authorRepository = authorRepository;
    }

    @PostMapping
    public String save(@RequestBody Author author){
        try {
            authorRepository.save(author);
            return "Successful insert";
        } catch (Exception e) {
            e.printStackTrace();
            return "Unsuccessful insert";
        }
    }

    @GetMapping
    public List<Author> findAll(){
        return authorRepository.findAll();
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<Author> findById(@PathVariable("id") Long id) throws ResourceNotFoundException {
        Author value = authorRepository.findById(id);
        return ResponseEntity.ok().body(value);
    }

    @DeleteMapping("{id}")
    public String delete(@PathVariable(value = "id") Long id) {
        try {
            authorRepository.delete(id);
            return "Successful delete with id: "+ id;
        } catch (Exception e) {
            e.printStackTrace();
            return "Unsuccessful delete with id: "+ id;
        }
    }

    @PutMapping("{id}")
    public String update(@PathVariable(value = "id") Long id, @RequestBody Author newAuthor) {

        try {
            Author authorToUpdate = authorRepository.findById(id);

            authorToUpdate.setFirstName(newAuthor.getFirstName());
            authorToUpdate.setLastName(newAuthor.getLastName());
            authorToUpdate.setPenName(newAuthor.getPenName());
            authorToUpdate.setBirthDate(newAuthor.getBirthDate());
            authorToUpdate.setNationality(newAuthor.getNationality());

            authorRepository.update(authorToUpdate);
            return "Successful update with id: "+ id;
        } catch (Exception e) {
            e.printStackTrace();
            return "Unsuccessful update with id: "+ id;
        }
    }
}
