package hu.ricky.books.repository;

import hu.ricky.books.model.Book;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends CoreCrudRepository<Book>{ }
