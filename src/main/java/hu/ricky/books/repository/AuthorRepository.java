package hu.ricky.books.repository;

import hu.ricky.books.model.Author;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository extends CoreCrudRepository<Author>{ }
