package hu.ricky.books.repository;

import hu.ricky.books.model.Author;
import org.springframework.stereotype.Repository;

@Repository
public class AuthorRepositoryImpl extends CoreCrudRepositoryImpl<Author> implements AuthorRepository{

    @Override
    public Class<Author> getManagedClass() {
        return Author.class;
    }
}
