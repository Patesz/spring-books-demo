package hu.ricky.books.repository;

import hu.ricky.books.model.Book;
import org.springframework.stereotype.Repository;

@Repository
public class BookRepositoryImpl extends CoreCrudRepositoryImpl<Book> implements BookRepository{

    @Override
    public Class<Book> getManagedClass() {
        return Book.class;
    }
}
