package hu.ricky.books.repository;

import hu.ricky.books.exception.ResourceNotFoundException;
import hu.ricky.books.model.AbstractEntity;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.List;

public abstract class CoreCrudRepositoryImpl<T extends AbstractEntity> implements CoreCrudRepository<T> {

    public abstract Class<T> getManagedClass();

    @PersistenceContext
    protected EntityManager em;

    @Override
    public List<T> findAll()   {
       return em.createQuery("select n from " + getManagedClass().getSimpleName() + " n").getResultList();
    }

    @Override
    public T findById(Long id) throws ResourceNotFoundException  {
        T result = (T) em.find(getManagedClass(), id);
        if(result == null){
            throw new ResourceNotFoundException(getManagedClass().getName() + " not found for this id :: " + id);
        }
        return result;
    }

    @Transactional
    @Override
    public void save(T entity) throws Exception {
        entity.setCreationTime(LocalDateTime.now());
        entity.setUpdateTime(LocalDateTime.now());
        em.persist(entity);
    }

    @Transactional
    @Override
    public void delete(Long id) throws ResourceNotFoundException{
        em.remove(findById(id));
    }

    @Transactional
    @Override
    public void update(T entity) throws ResourceNotFoundException {
        entity.setUpdateTime(LocalDateTime.now());
        em.merge(findById(entity.getId()));
    }
}
