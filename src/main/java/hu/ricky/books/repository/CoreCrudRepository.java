package hu.ricky.books.repository;

import hu.ricky.books.exception.ResourceNotFoundException;
import hu.ricky.books.model.AbstractEntity;

import java.util.List;

public interface CoreCrudRepository<T extends AbstractEntity> {
    List<T> findAll();

    T findById(Long id) throws ResourceNotFoundException;

    void save(T entity) throws Exception;

    void delete(Long id) throws ResourceNotFoundException;

    void update(T entity) throws ResourceNotFoundException;
}
