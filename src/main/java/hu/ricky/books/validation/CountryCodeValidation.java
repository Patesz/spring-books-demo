package hu.ricky.books.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.*;
import java.util.regex.Pattern;

public class CountryCodeValidation implements ConstraintValidator<ISBN, String> {

    private static final List<String> allCountry = Collections.unmodifiableList(new ArrayList<String>() {{
        final String[] countries = Locale.getISOCountries();

        for (String countryCode : countries) {
            final Locale locale = new Locale("", countryCode);
            add(locale.getCountry());
        }
    }});

    @Override
    public void initialize(ISBN constraintAnnotation) { }

    @Override
    public boolean isValid(String countryCode, ConstraintValidatorContext constraintValidatorContext) {
        return allCountry.contains(countryCode);
    }
}
