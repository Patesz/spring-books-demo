package hu.ricky.books.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ISBNValidation.class)
public @interface ISBN {
    String message() default "Invalid ISBN number";
    Class<?>[] groups() default {};
    public abstract Class<? extends Payload>[] payload() default {};
}
