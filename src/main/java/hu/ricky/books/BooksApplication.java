package hu.ricky.books;

import hu.ricky.books.api.BookController;
import hu.ricky.books.model.Book;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication
public class BooksApplication {

	public static void main(String[] args) {
		SpringApplication.run(BooksApplication.class, args);

		/*final BookController bookRepository = new BookController();
		bookRepository.addBook(new Book(""));*/
	}

}
