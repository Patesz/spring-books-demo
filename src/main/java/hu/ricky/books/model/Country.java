package hu.ricky.books.model;

import hu.ricky.books.util.CountryUtil;

import java.util.Map;

public class Country {

    static class UnsupportedCountryException extends Exception{ }

    public String code;
    public String name;

    /**
     * A static factory method used for instantiating a new Country object.<br>
     * Invalid Country objects can not be represented
     * if param String is not equals with any of the country codes
     *
     * @param countryCode Valid or invalid country code
     * @return Valid Country object
     * @throws Country.UnsupportedCountryException This exception is thrown if parameter string is not a valid country code.
     */
    public static Country newInstance(final String countryCode) throws UnsupportedCountryException {
        final Map<String, String> allCountry = CountryUtil.getAllCountry();

        for (Map.Entry<String, String> entry : allCountry.entrySet()) {
            if(entry.getKey().equalsIgnoreCase(countryCode)){
                return new Country(entry.getKey().toUpperCase(), entry.getValue());
            }
        }
        throw new UnsupportedCountryException();
    }

    // Must not be directly instantiated.
    private Country(String code, String name){
        this.code = code;
        this.name = name;
    }

}
