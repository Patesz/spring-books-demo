package hu.ricky.books.model;

import hu.ricky.books.util.StringUtil;

/**
 * Contains fiction and nonfiction book genres.
 * <br>
 * See list here:
 * <a href="https://reference.yourdictionary.com/books-literature/different-types-of-books.html">
 * List of book genres</a>
 */
public enum Genre{
    ACTION_AND_ADVENTURE,
    ANTHOLOGY,
    CLASSIC,
    COMIC_AND_GRAPHIC_NOVEL,
    CRIME_AND_DETECTIVE,
    DRAMA,
    FABLE,
    FAIRY_TAIL,
    FAN_HYPHEN_FICTION,
    FANTASY,
    HISTORICAL_FICTION,
    HORROR,
    HUMOR,
    LEGEND,
    MAGICAL_REALISM,
    MYSTERY,
    MYTHOLOGY,
    REALISTIC_FICTION,
    ROMANCE,
    SATIRE,
    SCIENCE_FICTION,
    SHORT_STORY,
    SUSPENSE_SLASH_THRILLER,
    BIOGRAPHY_SLASH_AUTOBIOGRAPHY,
    ESSAY,
    MEMOIR,
    NARRATIVE_NONFICTION,
    PERIODICALS,
    REFERENCE_BOOK,
    SELF_HYPHEN_HELP_BOOK,
    SPEECH,
    TEXTBOOK,
    POETRY;

    /**
     * @return Returns the enum value in a normal readable form.<br>
     * For example: Genre.SELF_HYPHEN_HELP_BOOK.toString() returned as Self-help book
     */
    @Override
    public String toString() {
        return StringUtil.capitalize(super.toString()
                .replaceAll("_HYPHEN_", "-")
                .replaceAll("_SLASH_", "/")
                .replaceAll("_", " ")
                .toLowerCase());
    }

}


