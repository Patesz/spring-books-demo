package hu.ricky.books.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import hu.ricky.books.validation.ISBN;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;

@Entity
@Table(name = "book")
public class Book extends AbstractEntity {

    //@ISBN

    @Column(name = "isbn", length = 17, unique = true)
    @Pattern(regexp = "^(?:ISBN(?:-10)?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})" +
            "[- 0-9X]{13}$)[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$")
    private String isbn;

    @NotEmpty
    @Column(name = "title", length = 40)
    private String title;

    @Column(name = "pages", length = 5)
    private Integer pages;

    @Column(name = "written_in")
    private LocalDate writtenIn;

    @Column(name = "genre", length = 40)
    private Genre genre;

    public Book() { }

    public Book(@JsonProperty("isbn") String isbn,
                @JsonProperty("name") String title,
                @JsonProperty("pages") Integer pages,
                @JsonProperty("writtenIn") LocalDate writtenIn,
                @JsonProperty("genre") Genre genre) {
        super();
        this.isbn = isbn;
        this.title = title;
        this.pages = pages;
        this.writtenIn = writtenIn;
        this.genre = genre;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getTitle() {
        return title;
    }

    public Integer getPages() {
        return pages;
    }

    public LocalDate getWrittenIn() {
        return writtenIn;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public void setWrittenIn(LocalDate writtenIn) {
        this.writtenIn = writtenIn;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }


}
