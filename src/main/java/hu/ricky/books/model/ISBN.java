package hu.ricky.books.model;

import java.util.regex.Pattern;

public class ISBN {

    public static class InvalidIsbnException extends Exception { }

    /**
     * Enum with ISBN_10 and ISBN_13
     */
    enum IsbnType{
        ISBN_10,
        ISBN_13,
    }

    /**
     * ISBN number
     */
    public final String number;

    /**
     * Stores an ISBN enum value
     */
    public final IsbnType type;

    // Regex both for ISBN-10 and 13
    private static String isbn_10_13_regex = "^(?:ISBN(?:-1[03])?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})" +
            "[- 0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)" +
            "(?:97[89][- ]?)?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$";

    private static String isbn_10_regex = "^(?:ISBN(?:-10)?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})" +
            "[- 0-9X]{13}$)[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$";

    private static String isbn_13_regex = "^(?:ISBN(?:-13)?:? )?(?=[0-9]{13}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)" +
            "97[89][- ]?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9]$";

    /**
     * A static factory method used for instantiating a new ISBN object.<br>
     * Invalid ISBN objects can not be represented if ISBN number validation fails during regex match.
     *
     * @param isbn Valid or invalid ISBN number
     * @return Valid ISBN object, which is either ISBN-10 or 13
     * @throws InvalidIsbnException This exception is thrown if parameter string is not a valid ISBN number.
    */
    public static ISBN newInstance(String isbn) throws InvalidIsbnException {
        final Pattern pattern10 = Pattern.compile(isbn_10_regex);
        final Pattern pattern13 = Pattern.compile(isbn_13_regex);

        final boolean matches10 = pattern10.matcher(isbn).matches();
        final boolean matches13 = pattern13.matcher(isbn).matches();

        if(!matches10 && !matches13){
            throw new InvalidIsbnException();
        } else {
            return new ISBN(isbn, matches10 ? IsbnType.ISBN_10 : IsbnType.ISBN_13);
        }
    }

    // Must not be directly instantiated.
    private ISBN(String number, IsbnType type){
        this.number = number;
        this.type = type;
    }

}
