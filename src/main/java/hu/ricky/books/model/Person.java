package hu.ricky.books.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import hu.ricky.books.validation.CountryCode;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotEmpty;
import java.time.DateTimeException;
import java.time.LocalDate;

@MappedSuperclass
public class Person extends AbstractEntity {

    @NotEmpty
    @Column(name = "first_name", length = 40)
    private String firstName;

    @NotEmpty
    @Column(name = "last_name", length = 40)
    private String lastName;

    @Column(name = "birth_date")
    private LocalDate birthDate;

    @CountryCode
    @Column(name = "nationality", length = 2)
    private String nationality;

    public Person(){}

    public Person(@JsonProperty("firstName") String firstName,
                  @JsonProperty("lastName") String lastName,
                  @JsonProperty("birthDate") String birthDate,
                  @JsonProperty("nationality") String nationality) throws Country.UnsupportedCountryException, DateTimeException {
        super();
        this.firstName = firstName;
        this.lastName = lastName;

        final String[] ymd = birthDate.split("[-/.]");

        this.birthDate = LocalDate.of(Integer.parseInt(ymd[0]), Integer.parseInt(ymd[1]), Integer.parseInt(ymd[2]));
        this.nationality = nationality;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public String getNationality() {
        return nationality;
    }

    public int getAge() {
        return LocalDate.now().getYear() - birthDate.getYear();
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

}
