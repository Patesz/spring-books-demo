package hu.ricky.books.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import hu.ricky.books.util.StringUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.DateTimeException;

@Entity
@Table(name = "author")
public class Author extends Person {

    @Column(name = "pen_name", length = 40, nullable = true)
    private String penName;

    public Author(){ super(); }

    public Author(@JsonProperty("firstName") String firstName,
                  @JsonProperty("lastName") String lastName,
                  @JsonProperty("birthDate") String birthDate,
                  @JsonProperty("nationality") String nationality,
                  @JsonProperty("penName") String penName) throws Country.UnsupportedCountryException, DateTimeException {
        super(firstName, lastName, birthDate, nationality);
        this.penName = penName;
    }

    boolean hasPenName(){
        return !StringUtil.isNullOrEmpty(penName);
    }

    public String getPenName() {
        return penName;
    }

    public void setPenName(String penName) {
        this.penName = penName;
    }
}

