package hu.ricky.books.util;

import hu.ricky.books.model.Genre;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GenreUtil{

    private static final List<String> allGenre = new ArrayList<String>() {{
        for (Genre g : Genre.values()) {
            add(g.toString());
        }
    }};

    /**
     * @return List of Genre represented as strings as an immutable collection
     *
     * @throws UnsupportedOperationException in case of object mutation
     */
    public static List<String> getAllGenreImmutable(){
        return Collections.unmodifiableList(allGenre);
    }

    /**
     * @return List of Genre represented as strings as a mutable collection
     */
    public static List<String> getAllGenreMutable(){
        return allGenre;
    }

}
