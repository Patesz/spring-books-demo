package hu.ricky.books.util;

import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

public class CountryUtil {

    private static final Map<String, String> allCountry = Collections.unmodifiableSortedMap(new TreeMap<String, String>() {{
        final String[] countries = Locale.getISOCountries();

        for (String countryCode : countries) {
            final Locale locale = new Locale("", countryCode);
            put(locale.getCountry(), locale.getDisplayCountry());
        }
    }});

    /**
     * @return Map of countries, KeySet contains country/region code ValueSet contains the name of countries.
     */
    public static Map<String, String> getAllCountry(){
        return allCountry;
    }

}
