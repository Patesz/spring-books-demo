package hu.ricky.books.util;

public class StringUtil {

    /**
     * @return Returns a new String with a capitalized first letter
     * if string is not null or empty.
     */
    public static String capitalize(String str) {
        if(str == null || str.isEmpty()) {
            return str;
        }

        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    /**
     * @return true: if string is null or empty<br>
     * false: if string is not empty
     */
    public static boolean isNullOrEmpty(String str){
        return (str == null || str.isEmpty());
    }
}
