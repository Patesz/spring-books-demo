package hu.ricky.books.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ISBNTests {

    @Test
    public void InstantiateValid() {
        try {
            final ISBN isbn = ISBN.newInstance("978-3-16-148410-0");

            assertEquals(isbn.type, ISBN.IsbnType.ISBN_13);
            assertEquals(isbn.number, "978-3-16-148410-0");
        } catch (ISBN.InvalidIsbnException e){
            fail("Test must fail if it somehow gets here.");
        }
    }

    @Test
    public void InstantiateInvalidThrowsException() {
        // Valid: 0-19-852663-6
        assertThrows(ISBN.InvalidIsbnException.class, () ->
                ISBN.newInstance("0-19-8663-6"));
    }

}