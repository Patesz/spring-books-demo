package hu.ricky.books.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CountryTests {

    @Test
    public void InstantiateValid() {
        try {
            final Country c = Country.newInstance("HU");

            assertEquals(c.code, "HU");
            assertEquals(c.name, "Hungary");
        } catch (Country.UnsupportedCountryException e){
            fail("Test must fail if it somehow gets here.");
        }
    }

    @Test
    public void InstantiateInvalidThrowsException() {
        assertThrows(Country.UnsupportedCountryException.class, () ->
                Country.newInstance("QK"));
    }

}